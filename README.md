#Authentication
An expressjs application using passport to auth users via JWT

#Tech stack
| Auth
| Express.js
| Node.js
| JWT
| Passport.js |

#How to run
Open console and run 

`npm install`

`npm run dev`